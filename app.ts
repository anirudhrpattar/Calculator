const number1 = document.getElementById("num1") as HTMLInputElement
const number2 = document.getElementById("num2") as HTMLInputElement

const AddBtn = document.getElementById("Add") as HTMLInputElement
const SubBtn = document.getElementById("Sub") as HTMLInputElement
const ProBtn = document.getElementById("Pro") as HTMLInputElement
const DivBtn = document.getElementById("Div") as HTMLInputElement
const ModBtn = document.getElementById("Mod") as HTMLInputElement

const Result = document.getElementById("result") as HTMLInputElement

function AddNumbers() {
    let a = parseFloat(number1.value);
    let b = parseFloat(number2.value);
    let result = a + b;
    Result.textContent = result.toString()
}
AddBtn.addEventListener("click", AddNumbers)

function SubNumbers() {
    let a = parseFloat(number1.value);
    let b = parseFloat(number2.value);
    let result = a - b;
    Result.textContent = result.toString()
}
SubBtn.addEventListener("click", SubNumbers)

function ProNumbers() {
    let a = parseFloat(number1.value);
    let b = parseFloat(number2.value);
    let result = a * b;
    Result.textContent = result.toString()
}
ProBtn.addEventListener("click", ProNumbers)

function DivNumbers() {
    let a = parseFloat(number1.value);
    let b = parseFloat(number2.value);
    let result = a / b;
    Result.textContent = result.toString()
}
DivBtn.addEventListener("click", DivNumbers)

function ModNumbers() {
    let a = parseFloat(number1.value);
    let b = parseFloat(number2.value);
    let result = a % b;
    Result.textContent = result.toString()
}
ModBtn.addEventListener("click", ModNumbers)