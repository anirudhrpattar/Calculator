// Paul was here
var number1 = document.getElementById("num1");
var number2 = document.getElementById("num2");
var AddBtn = document.getElementById("Add");
var SubBtn = document.getElementById("Sub");
var ProBtn = document.getElementById("Pro");
var DivBtn = document.getElementById("Div");
var ModBtn = document.getElementById("Mod");
var Result = document.getElementById("result");
function AddNumbers() {
    var a = parseFloat(number1.value);
    var b = parseFloat(number2.value);
    var result = a + b;
    Result.textContent = result.toString();
}
AddBtn.addEventListener("click", AddNumbers);
function SubNumbers() {
    var a = parseFloat(number1.value);
    var b = parseFloat(number2.value);
    var result = a - b;
    Result.textContent = result.toString();
}
SubBtn.addEventListener("click", SubNumbers);
function ProNumbers() {
    var a = parseFloat(number1.value);
    var b = parseFloat(number2.value);
    var result = a * b;
    Result.textContent = result.toString();
}
ProBtn.addEventListener("click", ProNumbers);
function DivNumbers() {
    var a = parseFloat(number1.value);
    var b = parseFloat(number2.value);
    var result = a / b;
    Result.textContent = result.toString();
}
DivBtn.addEventListener("click", DivNumbers);
function ModNumbers() {
    var a = parseFloat(number1.value);
    var b = parseFloat(number2.value);
    var result = a % b;
    Result.textContent = result.toString();
}
ModBtn.addEventListener("click", ModNumbers);
